﻿namespace FsSuccinctly.BouncingBall

/// Represents a ball by its position and its velocity
type Ball = 
    { X: float
      Y: float
      XVel: float
      YVel: float }

      /// convient function to create a new instance of a ball
        static member New(x: float, y: float, xVel: float, yVel: float) = 
            { X = x
              Y = y
              XVel = xVel
              YVel = yVel }

                   /// Create's a new ball that has moved one step by its velocity within 
        /// the given bounds, if the bounds are crossed the ball velocity is 
        /// inversed
        member ball.Move(xMin: float, yMin: float, xMax: float, yMax: float) =
            // local helper to implement the logic of the movement
            let updatePositionAndVelocity pos vel min max =
                let pos' = pos + vel
                if min < pos' && pos' < max then
                    pos', vel
                else
                    pos - vel, -vel
            // get the new position and velocity
            let newX, newXVel = 
                updatePositionAndVelocity ball.X ball.XVel xMin xMax
            let newY, newYVel = 
                updatePositionAndVelocity ball.Y ball.YVel yMin yMax

            // create the next ball
            Ball.New(newX, newY,newXVel, newYVel)
