﻿// *** NOTE - Use F# Interactive to test the code. SelectAll (CTRL+A) and F# Interactive (ALT + Enter)

#load "BallModel.fs"
open FsSuccinctly.BouncingBall

let genericBallTest name x y shouldBounceX shouldBounceY =
    // create a ball
    let ball = Ball.New(x, y, 1., 1.)
    // move the ball
    let ball' = ball.Move(0., 0., 100., 100.)
    // verify we have movement we expect
    let xOp = if shouldBounceX then (-) else (+)
    let yOp = if shouldBounceY then (-) else (+)
    if xOp ball.X ball.XVel <> ball'.X then failwith "X value not updated correctly"
    if yOp ball.Y ball.YVel <> ball'.Y then failwith "Y value not updated correctly"
    // notify user that the test has passed
    printfn "passed - %s" name

let testNoneBounce() =
    genericBallTest "testNoneBounce" 10. 10. false false

let testBounceX() =
    genericBallTest "testBounceX" 99. 10. true false

let testBounceY() =
    genericBallTest "testBounceY" 10. 99. false true

let testBounceBoth() =
    genericBallTest "testBounceBoth" 99. 99. true true


testNoneBounce()
testBounceX()
testBounceY()
testBounceBoth()