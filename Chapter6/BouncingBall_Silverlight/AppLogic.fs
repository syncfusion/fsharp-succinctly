﻿namespace FSharpSilverlightMvvmTemplate

    open System
    open System.Collections.Generic
    open System.Linq
    open System.Net
    open System.Windows
    open System.Windows.Controls
    open System.Windows.Documents
    open System.Windows.Input
    open System.Windows.Media
    open System.Windows.Media.Animation
    open System.Windows.Shapes
    open System.Windows.Media.Imaging
    open System.Windows.Threading

    [<AutoOpen>]
    module private Utilities = 
        /// Use this implementation of the dynamic binding operator
        /// to bind to Xaml components in code-behind, see example below
        let (?) (c:obj) (s:string) =
            match c with 
            | :? ResourceDictionary as r ->  r.[s] :?> 'T
            | :? Control as c -> c.FindName(s) :?> 'T
            | _ -> failwith "dynamic lookup failed"

            /// Represents a ball by its position and its velocity
    type Ball = 
        { X: float
          Y: float
          XVel: float
          YVel: float }

      /// convient function to create a new instance of a ball
        static member New(x: float, y: float, xVel: float, yVel: float) = 
            { X = x
              Y = y
              XVel = xVel
              YVel = yVel }

        /// Create's a new ball that has moved one step by its velocity within 
        /// the given bounds, if the bounds are crossed the ball velocity is 
        /// inversed
        member ball.Move(xMin: float, yMin: float, xMax: float, yMax: float) =
            // local helper to implement the logic of the movement
            let updatePositionAndVelocity pos vel min max =
                let pos' = pos + vel
                if min < pos' && pos' < max then
                    pos', vel
                else
                    pos - vel, -vel
            // get the new position and velocity
            let newX, newXVel = 
                updatePositionAndVelocity ball.X ball.XVel xMin xMax
            let newY, newYVel = 
                updatePositionAndVelocity ball.Y ball.YVel yMin yMax

            // create the next ball
            Ball.New(newX, newY,newXVel, newYVel)


    type BallRender() as br =
        inherit UserControl()
        // ball size and limits
        let size = 5.0
        let xMax = 100.0
        let yMax = 100.0
    
        // offset to give a nice board
        let offset = 10.0

        // calculate total width and height of the area
        let xTotalWidth = (xMax * size) + (offset * 2.0) |> int
        let yTotalWidth = (yMax * size) + (offset * 2.0) |> int

        // the writable bitmap which will allows to draw pixels on to it
        let bitmap = new WriteableBitmap(xTotalWidth,  
                                     yTotalWidth)

        // the image control that holds the bimap
        let image = new Image(Source = bitmap)

        // convert a color RBG to the int format used by the bitmap
        let colorToInt (c: Color): int =
         [c.A, 24; c.R, 16; c.G, 8; c.B, 0]
          |> List.sumBy (fun (col, pos) -> (int col) <<< pos)
        
         // function to set a pixel by it x y coordinates to the given color
        let setPixel x y c = bitmap.Pixels.[y * xTotalWidth + x] <- colorToInt c     

           // template that describes what the ball should look like
        let ballTemplate =
          [ "     "
            " *** "
            "*****"
            " *** "
            "     " ]

        // draws the ball on the bitmap
        let drawBall ball c =
            // calculate top corner of the ball
            let xBall = (ball.X * size) + offset
            let yBall = (ball.Y * size) + offset
            // iterate over the template setting a
            // pixel if there's a stars character
            ballTemplate
            |> Seq.iteri(fun x row ->
                row |> Seq.iteri (fun y item ->
                    if item = '*' then 
                        setPixel (int xBall + x)  (int yBall + y) c))

             // draws a square around the border of the area
        let drawSquare c =
            // calculate where sides should end
            let xSize = xMax * size + offset |> int
            let ySize = yMax * size + offset |> int
            // convert the offset to an int
            let offset = offset |> int
            // draw the x sides
            for x in offset .. xSize do
                setPixel x  offset c
                setPixel x  ySize c
            // draw the y sides
            for y in offset .. ySize do
                setPixel offset y c
                setPixel xSize y c

         // a reference cell that holds the current ball instance
        let ball = ref (Ball.New(50., 80., 0.75, 1.25))

        // timer for updating the balls position
        let timer = new DispatcherTimer(Interval = new TimeSpan(0,0,0,0,50))



        // handler for the timer's tick event
        let onTick() = 
            // write over the old ball
            drawBall !ball Colors.Black
            // move the current ball to its next position 
            ball := ball.Value.Move(0.,0.,xMax,yMax)

            // draw ball & square
            drawBall !ball Colors.Red
            drawSquare Colors.Red

            // invalidate the bitmap to force a redraw
            bitmap.Invalidate()


      // helper function to do some initialization
        let init() =
            // first color our bitmap black
            Array.fill 
                bitmap.Pixels 
                0 bitmap.Pixels.Length 
                (colorToInt Colors.Black)

            //make the image the user control's content
            br.Content <- image

            // set up the timer
            timer.Tick.Add(fun _ -> onTick())
            timer.Start()

        do init()

    type App() as this = 
       inherit Application()
       do this.Startup.Add(fun _ -> this.RootVisual <- new BallRender())