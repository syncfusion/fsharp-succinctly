﻿module Program3

// define an organization type
type Organization = { chief: string; indians: string list }

// create an instance of this type
let wayneManor = 
    { chief = "Batman"; 
      indians = ["Robin"; "Alfred"] }

// create a modified instance of this type
let wayneManor' = 
    { wayneManor with indians = [ "Alfred" ] }

// print out the two organizations
printfn "wayneManor = %A" wayneManor
printfn "wayneManor' = %A" wayneManor'
    
